#!/bin/sh

project_dir="$(dirname $0)/.."
actual="$($project_dir/src/hello.sh)"
expected="Hello world"

if [ "$actual" = "$expected" ]; then
    echo "The test passed. Yay!"
else
    echo "The test failed"
    exit 1
fi
